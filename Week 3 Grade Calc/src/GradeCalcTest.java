import junit.framework.TestCase;

public class GradeCalcTest extends TestCase {

	// Test Number: 1
		// Test Objective: To test conversion of CW Mark 23 and Exam Mark 70 to Grade A
		// Test input(s): "EM: 70 CW: 23"
		// Test expected output(s): 'A'
		
		public void testgenerate_grading001() {
			
			assertEquals("A", GradeCalc.generate_grading(23,70));
		}
		
		

		// Test Number: 2
			// Test Objective: To test conversion of CW Mark 22 and Exam Mark 30 to Grade B
			// Test input(s): "EM: 30 CW: 22"
			// Test expected output(s): 'B'
			
			public void testgenerate_grading002() {
				
				assertEquals("B", GradeCalc.generate_grading(22,30));
			}
			

			// Test Number: 3
			// Test Objective: To test conversion of CW Mark 24 and Exam Mark 20 to Grade C
			// Test input(s): "EM: 20 CW: 24
			// Test expected output(s): 'C'
				
			public void testgenerate_grading003() {
					
				assertEquals("C", GradeCalc.generate_grading(24,20));
			}
				

				// Test Number: 4
				// Test Objective: To test conversion of CW Mark 10 and Exam Mark 15 to Grade A
				// Test input(s): "EM: 15 CW: 10"
				// Test expected output(s): 'D'
					
			public void testgenerate_grading004() {
						
				assertEquals("D", GradeCalc.generate_grading(10,15));
			}
					

		
			
			// Test Number: 5
			// Test Objective: To test conversion of CW Mark -10 and Exam Mark -15 to Grade FM
			// Test input(s): "EM: -15 CW: -10"
			// Test expected output(s): 'FM'
				
		public void testgenerate_grading005() {
					
			assertEquals("FM", GradeCalc.generate_grading(-10,-15));
		}
				

		// Test Number: 6
		// Test Objective: To test conversion of CW Mark 110 and Exam Mark 115 to Grade FM
		// Test input(s): "EM: 115 CW: 110"
		// Test expected output(s): 'FM'
						
		public void testgenerate_grading006() {
							
			assertEquals("FM", GradeCalc.generate_grading(110,115));
		}
						
						
		// Test Number: 7
		// Test Objective: To test conversion of CW Mark 30 and Exam Mark 70 to Grade FM
		// Test input(s): "EM: 70 CW: 30"
		// Test expected output(s): 'FM'
								
		public void testgenerate_grading007() {
									
			assertEquals("FM", GradeCalc.generate_grading(30,70));
		}
		
		// Test Number: 8
		// Test Objective: To test conversion of CW Mark 10 and Exam Mark 90 to Grade FM
		// Test input(s): "EM: 90 CW: 10"
		// Test expected output(s): 'FM'
										
		public void testgenerate_grading008() {
											
			assertEquals("FM", GradeCalc.generate_grading(10,90));
		}
		

	
	
}


